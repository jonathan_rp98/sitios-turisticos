package com.colomTours.sitiosTuristicos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colomTours.sitiosTuristicos.dao.api.SitioJDBCDaoAPI;
import com.colomTours.sitiosTuristicos.model.Sitio;
import com.colomTours.sitiosTuristicos.service.api.SitioServiceAPI;

@Service
public class SitioServiceImpl implements SitioServiceAPI {

	private SitioJDBCDaoAPI sitioJDBCDaoAPI;

	@Autowired
	public SitioServiceImpl(SitioJDBCDaoAPI sitioJDBCDaoAPI) {
			super();
			this.sitioJDBCDaoAPI = sitioJDBCDaoAPI;
	}
	



	@Override
	public void save(Sitio sitio) {
		sitioJDBCDaoAPI.save(sitio);
	}

	@Override
	public List<Sitio> getAll() {
		return sitioJDBCDaoAPI.getAll();
	}

	@Override
	public void delete(Integer id) {
		sitioJDBCDaoAPI.delete(id);
	}

	
	
	@Override
	public void update(Sitio sitio) {
		sitioJDBCDaoAPI.update(sitio);
	}

	@Override
	public List<Sitio> findByNombre(String id) {
		
		return sitioJDBCDaoAPI.findByNombre(id);
	}
	
	@Override
	public void save_image(String url, int id_sitio) {
		sitioJDBCDaoAPI.save_img(url, id_sitio);
	}
	
	@Override
	public void update_guia(int id_sitio, String url) {
		sitioJDBCDaoAPI.update_guia(id_sitio, url);
	}


}
