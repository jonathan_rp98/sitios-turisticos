package com.colomTours.sitiosTuristicos.service.api;

import java.util.List;
import com.colomTours.sitiosTuristicos.model.Sitio;


public interface SitioServiceAPI {

	void save(Sitio sitio);

	List<Sitio> getAll();

	void delete(Integer id);

	void update(Sitio sitio);

	List<Sitio> findByNombre(String nombre);
	
	void save_image(String url, int id_sitio);

	void update_guia(int id_sitio, String url);
}
