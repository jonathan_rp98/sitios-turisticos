package com.colomTours.sitiosTuristicos.service.api;

import java.util.List;



public interface SeviceAPI <S,T>{
	void save(S objecto);

	List<S> getAll();

	void delete(T id);

	void update(S objecto);
}
