package com.colomTours.sitiosTuristicos.dao.api;

import java.util.List;

import com.colomTours.sitiosTuristicos.model.Sitio;

public interface SitioJDBCDaoAPI {

	void save(Sitio sitio);
	List<Sitio> getAll();

	void delete(Integer id);

	void update(Sitio sitio);

	List<Sitio> findByNombre(String id);
	
	void save_img(String url, int id_sitio);

	void update_guia(int id_sitio, String url);
}
